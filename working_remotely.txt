The Keys to Working Remotely - Carson Shold

- Remote: Office Not Required (book)
- A Year Without Pants (book)
- FlexJobs (job board)
- We Work Remotely (already RSS'd)

- WFH is not:
-- Free pass to do nothing
-- A burden on communication - many tools to make this work, so long as everybody invested
-- Either/or.
-- Can be done from home.

- WFH is:
-- Being out of office
-- Trust is necessary.  Some companies lack trust in employees.
-- Just work.  Not really different.

- Open floor plans are quiet despite desire to have impromptu discussion, because people communicate better on-screen.
- http://justinjackson.ca/remote-work/
- Remote employment is going up, or at least number of companies with remote workers is going up.

- Perks:
-- Cost of living doesn't need to match company location
-- Flexible hours
-- Removal of distractions
--- Fewer impromptu discussions, fewer meetings.
--- Fewer interruptions.
---- Jason Fried - Why work doesn't happen at work
-- Fewer office politics
-- Forces team to practice communication
-- Not tied to any desk
-- Pants optional

- Downsides:
-- Out of sight, out of mind
--- Get a chromebox for meetings
-- Less social interaction (can't rely on work for social)
-- "When are you in town next?"  People unnecessarily waiting for face-to-face.
-- The Guilt - the extra freedom
-- Being unable to Turn Work Off.
--- Good to have a separate work space you can turn off, close, etc.
-- Non-work distractions increase.

- How To:
-- Experiment w/ workplaces:
--- Shared workspaces
--- Coffeeshops
--- Home office
-- Over-Communicate
--- "Do something and tell people"
--- Weekly/monthly updates to team lead or team in general.
-- Ask for feedback
--- CRs are probably good.
--- Design reviews
--- "How am I doing?" - ask the tech lead, check-in, etc.
-- Team Building
--- Get some non-work events in with team members.
-- Be Available (during work hours)
--- Late replies amplify remote working problems.
-- Have a Routine
--- Do a thing to indicate start of work day and end of work day.
--- Schedule blocks of work time.
---- E.g. Do CRs and PRs first thing, then emails, then uninterrupted work

- Managing remote team
-- Give trust
-- Base compensation on talent, not location.  Don't pay based on cost-of-living locally.
-- Do 1:1s periodically to keep everybody up-to-date.
-- Keep things level.
--- Don't give just some people rights to work remote.
--- Keep remote devs involved, don't mute them or forget to invite them...
-- Start slow
--- Work remotely first
--- Or let people take a few days.
-- Provide a home office budget for remote devs.

- QA
-- Could be good for diversity, if you're careful.  International, different regions, don't hire just white dudes.
-- Finding a job when specifically wanting remote work?
--- During initial interview, probably not during recruitment phase.  Recruiters will toss you aside.
-- Microphone/headset:
--- Earbuds+mic pretty important for remote user.
--- Invest in a good mic at office.  Conference mic, omnidirectional, etc.
-- How to onboard remote junior devs:
--- Fly them in for a training period.
--- Constant 1:1 chat with a mentor.
--- Video calls are good.

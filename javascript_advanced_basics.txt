- Two object types:
-- value types
--- ints, strings, etc
--- functions are pass-by-value (a copy) for these types
-- reference types
--- functions are pass-by-reference for these types

- Globals can be modified directly for values and references.
-- declaring a var inside a function can shadow the value and prevent changes
-- JavaScript does "hoisting", where all variables are defined near the top (but assignments are not carried out until the line is executed).

e.g.:
var a = 0;

function foo(){
  a++;
  console.log(a); //undefined
  var a=0;
}
console.log(a); // prints 0, shadowing still happens

- A closure is a function with a reference to a variable not defined within its own scope. (Er, a closure is created in this case).
- Environments:
-- Variable envs are defined during parsing.
-- Variable assignments are executed during execution.
-- Variables defined higher in chain have broader domain, but lower priority.
-- Namespaces are good.  Namespace collisions are bad.
-- Use objects for namespaces.
- Envs look like:

{a:0, b:3, '..':  #inner-most scope, '..' means parent
  {a: 43, '..':
    {...}
  }
}

So lookups cascade into parents until a ref is found.

- Functions:
-- Currying, where one function takes one arg and returns a function that takes the next arg.  Results in things that looks like add(2)(5).
--- Python calls these partials?
-- Functions can be arguments (like any other functional language).
--- Including functions with closures.

- Callback Pyramids
-- When callbacks all call anonymous functions by giving them names.
-- This is hard to decompose if state is added during the callback pyramid.
--- Fix this with Javascript's built-in .call() and .apply().

.call() and .apply()
- allows execution of any function within any environment.
- call() takes one parameter, the env (object, e.g. 'this') to run the function in.  Further args are passed one-to-one to the function.
- apply() takes the env and then an additional array/object of parameters to pass to the function.
- You can get at unaccounted (extra) args via Array.prototype.slice on the magical 'arguments' var in every function.
- The inside a function, this.length is the number of args passed in.

var __slice = Array.prototype.slice

function addNumToArray(num){
  var to_incr = __slic.call(arguments, this.length);
  map(to_incr, add(num));
}

- Classes
-- Javascript classes are objects with two requirements:
--- Constructors
---- Constructors are functions and functions are constructors.  The 'new' keyword results in the function returning an object.
---- var things = new randomThings();  //calls the function, but replaces the return statement with a return the context for the function.
---- variables declared within the constructor scope are not available; need to assign them to this for them to stick around as member vars.
---- Can use function.call(this) inside a constructor to call other functions, to bind those functions' "this" to the constructor's new object.
---- But really just use this.my_method = function() to make the function available outside the constructor.
--- Prototypes
---- Prototypes are just objects.
----- Classname.prototype = object;  Object can be a hash of functions, which is basically how inheritance works.  
----- If you modify the prototype of one class, you modify the prototype of all classes.  Instead do Subclass.prototype = Object.new(Parentclass.prototype); to avoid the problem.




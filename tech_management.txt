- Almost all management sucks.
- see slides for definition(s)

- "Management is the art of getting things done through people." (Mary Parker Follett)
-- craft, not art

- Craft
-- needs organization skills, office politics, personal interactions

- GTD
-- need to know tech, morket for the tech, hiring/firing, budget (for all resources), company strategy (etc)

- "You are certified by the community you serve."

- People
-- managed need to know not just what they're doing but why
-- transparency very important
--- except for things you can't be transparent about
-- treat feedback for good/bad events like bug reports - specifics are needed
-- need to communicate expectations
--- but avoid superfluous rules or processes
-- communicate consequences (good/bad) for expectations

- Fix processes iteratively
-- and measure changes to make sure they're positive

- Bob Sutton - Work Matters blog

- VM Brasseur 
- shoeless consulting

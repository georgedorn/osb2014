Things scikit-learn does:
- Classification
-- E.g. evernote uses this for document classification.  Uses naive bayes for this.
- Clustering
- Regression
- Dimensional reduction

Supervised vs Unsupervised learning
- Supervised means you are providing data labels.  Unsupervised means the model attempts to determine labels.

- Sample size high, featureset low.

k-NN is supervised (user input is labels)
- Lazy algorithm, doesn't do computation until a new data point is added.
- Find three closest neighbors (in features), majority vote wins.
-- Equal weight or weighted distance.

jvns.ca/blog/2013/12/22/cooking-with-pandas
nbviewer.ipython.org/github/cs109/content/blob/master/lec_04_wrangling.ipynb

Kaggle - machine learning competition (for money or jobs) and has good docs.
See PyCon 2014 on pyvideo.org Kaggle.

- see http://github.com/pkafei/Know_Thy_Neighbor.git



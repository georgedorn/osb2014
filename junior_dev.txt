Who?
- solid understanding of one language and basic algos, but little-to-no real-world experience.

Why?

Sustainable hiring
- 27% job growth in software in OR in next decade
Less tech debt

common wisdom
- can't spare mentors
- bad docs
- incomplete tests
- juniors take too long to provide value
- idealized view of hor company should be to hire juniors, which never happens

essentials:
- understand your situation, find right candidate to fit org

Not Altruism
- Not for altruism.  Find someone that will provide value for your specific situation.
- Juniors make $56k, Seniors make $107k
- W/ recruiting and training costs, Juniors are about 60k less for first year.
- Onboarding costs about 33% of employee cost.
-- The longer a dev stays, the less the onboarding costs per year.

Define 'Value':
- not just  shipping code
- juniors are great for flushing out process issues, by asking questions about every problem they run into.  that is value.
- can assign juniors to fix pain points, even if you scrap their solution, it starts a convo.
- juniors can be assigned to low-priority bugs that aren't worth spending a senior dev's salary on

Case Study:
- team isn't large enough to spare seniors for mentoring
-- what is your actual capacity?
-- let the candidate know about how much mentoring they are likely to get.

2:
- important that every dev provide value asap
-- what is 'value'?
-- juniors are like cats - get 2 of them

Review:
cheaper
more valuable than expected
more available than seniors

How:
- communicate expectations
- dedicate mentoring in amount needed to reach onboard goal

Portland Code School
- for hiring devs

"We only hire senior devs" blog post
"onboarding, training, mentoring" pycon video

Juniors are probably less likely to leave in a year, esp if you can give them raises as they gain XP, and are less likely to be poached.

Mentoring should be monitored, don't just throw seniors in and hope for the best.

Juniors can be created from within, if you have QA or ops.
Hire juniors for longer contracts b/c they don't have cushions and will be jobhunting instead of working on short contracts.





